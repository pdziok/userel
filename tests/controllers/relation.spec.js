var RelationController = require('../../api/controllers/RelationController'),
    sinon = require('sinon'),
    assert = require('assert');

describe('The Relation Controller', function () {
    describe('when we load the about page', function () {
        it ('should render the view', function () {
            var view = sinon.spy(),
                req = sinon.mock();
            RelationController.get(req, {
                view: view
            });
            assert.ok(view.called);
        });
    });
});