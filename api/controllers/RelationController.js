/**
 * RelationController
 *
 * @module      :: Controller
 * @description	:: A set of functions called `actions`.
 *
 *                 Actions contain code telling Sails how to respond to a certain type of request.
 *                 (i.e. do stuff, then send some JSON, show an HTML page, or redirect to another URL)
 *
 *                 You can configure the blueprint URLs which trigger these actions (`config/controllers.js`)
 *                 and/or override them with custom routes (`config/routes.js`)
 *
 *                 NOTE: The code you write here supports both HTTP and Socket.io automatically.
 *
 * @docs        :: http://sailsjs.org/#!documentation/controllers
 */

var RelationController = {
    
    add: function(req, res) {
        var antecedentId = parseInt(req.param('antecedent'));
        var consequentId = parseInt(req.param('consequent'));

        if (antecedentId != req.param('antecedent')) {
            return res.json({
                err: [
                    'Antecedent ID is not correct integer value, given: "' + req.param('antecedent') + '"'
                ]
            }, 500)
        }

        if (consequentId != req.param('consequent')) {
            return res.json({
                err: [
                    'Consequent ID is not correct integer value, given: "' + req.param('consequent') + '"'
                ]
            }, 500)
        }

        var addSingleRelation = function(antecedentId, consequentId, cb) {
            Relation.findOne({antecedent: antecedentId})
                .done(function(err, relation) {
                    if (err) return cb(err, []);
                    if (!relation) {
                        Relation.create({
                            antecedent: antecedentId,
                            consequent: [consequentId]
                        }, function(err, relation){
                            return cb(err, relation)
                        })
                    } else {
                        relation.addConsequent(consequentId)

                        relation.save(function(){
                            return cb(err, relation)
                        });
                    }

                });
        };


        addSingleRelation(consequentId, antecedentId, function(err) {
            if (err) {
                res.json(err, 500);
            }
        });

        addSingleRelation(antecedentId, consequentId, function(err, relation) {
            if (err) {
                res.json(err, 500);
            }

            res.json(relation.consequent)
        });
    },

    get: function(req, res) {
        var antecedentId = parseInt(req.param('antecedent'));

        if (antecedentId != req.param('antecedent')) {
            return res.json({
                err: [
                    'Antecedent ID is not correct integer value, given: "' + req.param('antecedent') + '"'
                ]
            }, 500)
        }

        Relation.findOne({antecedent: antecedentId})
            .done(function(err, relation) {
                if (err) return res.send(err,500);
                if (!relation) return res.json([]);
                return res.json(relation.consequent);
        });
    },

    remove: function(req, res) {
        var antecedentId = parseInt(req.param('antecedent').valueOf());
        var consequentId = parseInt(req.param('consequent').valueOf());

        if (antecedentId != req.param('antecedent')) {
            return res.json({
                err: [
                    'Antecedent ID is not correct integer value, given: "' + req.param('antecedent') + '"'
                ]
            }, 500)
        }

        if (consequentId != req.param('consequent')) {
            return res.json({
                err: [
                    'Consequent ID is not correct integer value, given: "' + req.param('consequent') + '"'
                ]
            }, 500)
        }

        var removeSingleRelation = function(antecedentId, consequentId, cb) {
            Relation.findOne({antecedent: antecedentId})
                .done(function(err, relation) {
                    if (err) return cb(err, []);
                    if (!relation) {
                        return cb(err, [])
                    } else {
                        relation.removeConsequent(consequentId);

                        relation.save(function(err, relation){
                            return cb(err, relation)
                        });
                    }

                });
        };

        removeSingleRelation(antecedentId, consequentId, function(err, relation){
            if (err) {
                res.json([], 500);
            }
            res.json(relation.consequent || [])
        });

        removeSingleRelation(antecedentId, consequentId, function(err, relation){
            if (err) {
                res.json([], 500);
            }
        });


    },



  /**
   * Overrides for the settings in `config/controllers.js`
   * (specific to RelationController)
   */
  _config: {}

  
};

module.exports = RelationController;