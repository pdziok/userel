/**
 * Relation
 *
 * @module      :: Model
 * @description :: A short summary of how this model works and what it represents.
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

var Relation = {

    types: {
        antecedent: function(antecedent){
            return this.consequent.indexOf(antecedent) === -1
        }
    },

    adapter: 'mongo',

    config: {
        database: 'userel',
        host: '127.0.0.1',
        port: 27017
    },

    attributes: {
        antecedent: {
            type: 'INTEGER',
            required: true,
            antecedent: true,
            index: true,
            integer: true
        },
        consequent: {
            type: 'array'
        },

        addConsequent: function(consequentId) {
            this.consequent.push(consequentId);

            return this;
        },

        removeConsequent: function(consequentId) {
            var index = this.consequent.indexOf(consequentId);
            if (index !== -1) {
                this.consequent.splice(index, 1);
            }
        }

    },

    beforeUpdate: function(values, next) {
        var unique = function(value, index, self) {
            return self.indexOf(value) === index;
        };

        values.consequent = values.consequent.filter(unique);

        next();
    },

    autoCreatedAt: false,
    autoUpdatedAt: false


};

module.exports = Relation;
