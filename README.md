# userel
## Overview
An API handling user relations o2o both side as seen as on social sites

## Requirements
* npm (http://nodejs.org/)
* sails (http://sailsjs.org/)
* mongoDB (http://www.mongodb.org)
* grunt (http://gruntjs.com/) - development only

## Installation

1. Clone repository
2. Install npm dependencies
3. Install and run MongoDB
4. Run the sails server (`sails lift`)

## Configuration

All necessary configuration could be found in `config/local.js`.

Example configuration:

    module.exports = {

        port: 1337,
        host: 'localhost',
        environment: 'production',
        adapters: {
            mongo: {
                host: 'localhost',
                post: 27017,
                database: 'userel'
            }
        }
    };

## Usage

Assuming default, non-changed configuration application runs at host http://localhost:1337/

### Retrieving user friends

There are 3 possible ways to do so:
1. [N/A] /?antecedent=UID
1. [N/A] /get/UID
1. [GET] /UID

That kind of request returns list of UIDs that are in relation to given antecedent UID. Weather antecedent UID is not known to the service it returns empty list

### Adding friends
1. [N/A] /add/?antecedent=UID1&consequent=UID2
1. [N/A] /add/UID1/UID2
1. [POST] /UID1/UID2

After adding a relation the service it will be returned current friend list of user with ID given as UID1

### Removing friends

1. [N/A] /remove/?antecedent=UID1&consequent=UID2
1. [N/A] /remove/UID1/UID2
1. [DELETE] /UID1/UID2

After adding a relation the service it will be returned current friend list of user with ID given as UID1